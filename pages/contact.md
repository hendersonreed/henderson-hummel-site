# Contact me

please feel free to email me at: reed [dot] hummel [at] gmail [dot] com

I am also available on the [Portland State University Slack channel](https://pdx-cs.slack.com) as "reed", should you have access to that medium - it's PSU students only, if I recall correctly.
