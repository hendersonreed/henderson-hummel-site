# Sorted in order of *descending* approachability

## Music from other places

* Buena Vista Social Club
* Nick Drake
* William Onyeabor
* Neil Halstead
* Sibylle Baier
* Sinkane
* Miramar
* Pentangle
* Bert Jansch (album: Avocet)
* Jethro Tull (album: The Best of Acoustic Jethro Tull)
* Devotchka
* Dengue Fever
* A Hawk and a Handsaw
* Amsterdam Klezmer Band
* The Klezmatics
* Jimmy Cliff (song: Guns of Brixton)
* The Congos
* DahkaBrakha
* Kila
* Pupajim
* Stand High Patrol
* Sudan Dudan

## Music from the US, mostly

* Kacy and Clayton
* Zephaniah Ohora
* Andrew Bird\'s Bowl of Fire
* Mariachi El Bronx
* Louis Cole
* Marty Robbins (album: Gunfighter Songs)
* David Byrne (album: American Utopia)
* Andy Shauf (album: The Party)
* Man Man
* Tom Waits (albums: Rain Dogs, Alice)
* Isaac Albeniz
* Swamp Dogg (album: Rat On!)
* Mort Garson (album: Plantasia)
* Leon Redbone
* Misophone
* Moondog (album: The German Years. Find on Youtube)
* Captain Beefheart (album: Safe as Milk)
