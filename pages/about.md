# I'm Henderson Reed Hummel

and welcome to my patch of the Internet.

I work as a Quality Engineer on Red Hat's Ansible Automation Platform team, writing and automating tests for some of the products in the Platform.

My interests include (but aren't limited to):

* functional programming
* programming language design
* the game of [Go](https://en.wikipedia.org/wiki/Go_(game))
* traditional wooden sailboats
* free and open source software (free as in Freedom)
* hand tool woodworking
* the beauty of the weird and whimsical web

I, together with a few friends, contribute to [Terra](https://terra.finzdani.net), a web directory we curate.

Check out some of the [music I like](/pages/music.html)

I've got some [book recommendations](/pages/books.html)

I love to cook, I'm currently adding some [recipes](/pages/recipes/) to this personal page.

If you're looking for software that I've written, check out my [Gitlab](https://gitlab.com/hendersonreed). The public work I do in my role at Red Hat is primarily on my [GitHub](https://github.com/hendersonreed).

Finally, if you're looking for the old website (there's some records of things I've done there) then you can find it [here](/old-site/index.html).
