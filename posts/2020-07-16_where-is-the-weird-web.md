# where is the weird web?
#### published 2020-07-16 {.date-listing}

Across the internet, it seems there is a growing community of people who are disappointed with the way the web has developed. I'll confess I find myself among their number, and maybe you are too.

So, where has the fun of the web gone? It still exists out there, in small pockets, untouched by the major search engines. I call it **the weird web** and it's a fun place to be.

## a confession

I grew up in a house with a home computer, but I'm young enough that I don't have a true-to-reality vision of what the web was like in the late 90s and early 2000s. So ultimately, I've developed a love for a time in which I never lived. Not quite nostalgia, but an appreciation for what has been lost with the commoditization of the web.

## nonetheless

In a sense, the fact that my perspective is **not** softened by nostalgia means something almost more significant. My appreciation for the simple technologies and light-hearted web creations of the past is born purely from interaction with them and the creations they inspired, not from a desire for the old days.

So, as someone free of the veil of nostalgia, what are the things I like about the weird web?

### simplicity

Now, with dynamic pages featuring node.js backends, MongoDBs, React frontends, and a plethora of other technologies, the *percieved* technical and aesthetic barrier to creating a webpage has gone way way up, despite the fact that there is so much fun and interesting stuff feasible with a simple static page.

Note above that I mentioned the *percieved aesthetic barrier*. What do I mean by this? Namely, the fact that, once, a simple HTML page created in a WYSIWYG editor was enough to satisfy the average internet creator and **now it is not**. There was a time when people of all ages were creating basic web pages because it was a fun way to broadcast your personality with the world. And when you come across a small page like that now, it stands outside of time, and it feels so much more personal than your standard facebook or instagram account.

why has this changed? the web has gotten more complex, and people's standards have changed. With beautiful new websites, people stopped trying to make their own --- it is so much easier and more convenient to sign up for $service and broadcast your personality there, in that walled garden. **Can this be reversed?** I'm not sure. I'd like to hope so. We see things like [NeoCities](https://neocities.org) which aim to bring some of the simplicity back. In a lot of ways it has succeeded, though it feels almost disjointed, as though people aren't really browsing and working together, only creating.

### whimsy

There's a whimsy in some of the weird web that's hard to see in the rest of the web. It resurges from time to time, and you see a flurry of activity before things settle down and die out once again. There's something missing for the sustainability of the weird web --- users are easily wooed away by the convenience of the bigger platforms, and the network effect on those platforms makes it really difficult to remain an independant creator.

Examples of the whimsy I'm talking about: 

* [special fish](https://special.fish)
* [kicks condor](https://kickscondor.com)
* [100 rabbits](https://100r.co)
* [the bus stop](https://bus-stop.net)

### the absence of commerce

One of the most transformational aspects of the web, this is a double-edged sword. Being able to tap a pattern into my handheld computer and recieve a pizza 30 minutes later is a shockingly powerful change for society. At the same time, the countless little commerce-informed changes (Google Adsense, user tracking, the banality of modern web design, search-engine optimization, the focus on commercial results by search engines) of the web have sentenced the weird web to the Death of a Thousand Cuts.

## What can you do?

Well, what do we want? 

In a lot of ways, what makes the weird web so neat is that it is small. So maybe we don't want to encourage everyone to go join special.fish or bother kickscondor (find them on [Terra](https://terra.finzdani.net)). In fact, I went back and forth multiple times about even linking to these special small places, fearing that a wider audience would somehow pollute them (or eradicate the warm fuzzy feeling we get from keeping a wonderful secret.) But if we stay selfish, and keep these projects to ourselves, maybe we do the world a disservice - we want others to see these things and be inspired.

In my opinion, we want to change the landscape of the web. Right now, there are a few metropolises, gargantuan platforms which house millions of people, surrounded by a few pinpricks of light. I'd encourage you to start your own pinprick of light, a teeny tiny bastion of weirdness here on the web. You can make your own Terra (it's open source! [gitlab](https://gitlab.com/waylon531/terra)), you can make an account on NeoCities, you can develop your own web experiments, and you can share these things with people all over when you find them.
