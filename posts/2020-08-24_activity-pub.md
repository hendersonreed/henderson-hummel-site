# Activity Pub

My friend [uelen](https://finzdani.net) set up a Pleroma instance, so now I've got another foot in the fediverse. I used to have a Mastodon account, but the community felt too large to really engage with. Maybe I'll write at some point on how size impacts internet communities. Suffice it to say right now that I've never really enjoyed contributing to communities that are too large.

Anyways, now I'm on Pleroma. It's an interesting place - I already have some ideas for some software that I want to write to liven the place up a little. It's currently just 3 of us, and due to a bug in Pleroma or our configuration we can't seem to follow remote instances. Those two facts make it a quiet place.

The most concrete thing I'm considering writing is a small bot account to watch the RSS feeds I hand it and post links when new items appear. Right now I do a lot of manual sharing of links, and sometimes there are sites I follow where I *always* repost the links to share with friends. So having a bot to watch and repost about a small selection of sites would be a pretty handy tool.

Anyways, that's all for right now! Until later.
